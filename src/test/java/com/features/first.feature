
@Regression
Feature: Testing the online fruits and veggies app

Background:
Given user launches tha app

@valid
Scenario: Valid credentials

When user gives valid credentials "Online_User" as username and "Online_User" as password
Then user see success message

@invalid
Scenario: Invalid credentials


When user gives invalid credentials "Onlineuser" as username and "Onlineuser" as password
Then user see error message


@invalidwithdata

Scenario Outline: Invalid credentials


When user gives invalid credential <uname> as username and <pass> as password
Then user see error messages

Examples:
|uname|pass|
|"user1"|"pass1"|
|"user2"|"pass2"|
