package com.steps;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
 
public class StepDefClass {
	
	
WebDriver driver;
@Before
public void beforeScenario(Scenario sc) {
String a=sc.getName();
Status b=sc.getStatus();
System.out.println(a);
System.out.println(b);
}

@After
public void afterScenario(Scenario sc) {
	boolean sts=sc.isFailed();
	if(sts==true) {
		System.out.println(sts);
	}
}
@Given ("^user launches tha app$")
public void user_launches_tha_app() throws Exception {
	//System.out.println("launching");
	System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe\\");
	driver=new ChromeDriver();
	driver.get("http://127.0.0.1:8080/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	
}


@When ("^user gives valid credentials \"([^\"]*)\" as username and \"([^\"]*)\" as password$")
public void validCredentials(String username, String password) {
	driver.findElement(By.xpath("//input[@name='uname']")).sendKeys(username);
	driver.findElement(By.xpath("//input[@name='psw']")).sendKeys(password);
	driver.findElement(By.xpath("//button")).click();
	System.out.println("i am adding this line in develop branch");
	System.out.println("i am adding this line in develop branch");
	System.out.println("i am adding this line in develop branch");

	

}
@Then ("^user see success message$")
public void successMsg() throws Exception {
Thread.sleep(5000);
driver.close();
	
}

@When ("^user gives invalid credentials \"([^\"]*)\" as username and \"([^\"]*)\" as password$")

public void invalidCredentials(String username,  String password) throws Exception {
	driver.findElement(By.xpath("//input[@name='uname']")).sendKeys(username);
	driver.findElement(By.xpath("//input[@name='psw']")).sendKeys(password);
	driver.findElement(By.xpath("//button")).click();
	
	Thread.sleep(3000);
	
}
@Then ("^user see error message$")
public void errorMsg() {
	Alert alert=driver.switchTo().alert();

	String actual=alert.getText();
	
	
	System.out.println(actual);
	alert.accept();
	
	driver.close();
}	
@When ("^user gives invalid credential (.*) as username and (.*) as password$")
public void invalidCredentialsdata(String uname, String pass) throws Exception {
	driver.findElement(By.xpath("//input[@name='uname']")).sendKeys(uname);
	driver.findElement(By.xpath("//input[@name='psw']")).sendKeys(pass);
	driver.findElement(By.xpath("//button")).click();
	
	Thread.sleep(3000);
}
@Then ("^user see error messages$")
public void errormagdata() {
	Alert alert=driver.switchTo().alert();
	String act=alert.getText();
	System.out.println(act);
	alert.accept();
	driver.close(); 
}
}
